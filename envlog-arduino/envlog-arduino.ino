#include <SPI.h>
#include <SD.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include <Adafruit_SGP30.h>
#include <Adafruit_AHTX0.h>

// SGP30
Adafruit_SGP30 sgp;
Adafruit_SSD1306 display = Adafruit_SSD1306(128, 32, &Wire);
// AHT20
Adafruit_AHTX0 aht;
Adafruit_Sensor *aht_humidity, *aht_temp;

// SGP30
/* return absolute humidity [mg/m^3] with approximation formula
* @param temperature [°C]
* @param humidity [%RH]
*/
uint32_t getAbsoluteHumidity(float temperature, float humidity) {
    // approximation formula from Sensirion SGP30 Driver Integration chapter 3.15
    const float absoluteHumidity = 216.7f * ((humidity / 100.0f) * 6.112f * exp((17.62f * temperature) / (243.12f + temperature)) / (273.15f + temperature)); // [g/m^3]
    const uint32_t absoluteHumidityScaled = static_cast<uint32_t>(1000.0f * absoluteHumidity); // [mg/m^3]
    return absoluteHumidityScaled;
}

// OLED FeatherWing buttons map to different pins depending on board:
#if defined(ESP8266)
#define BUTTON_A  0
#define BUTTON_B 16
#define BUTTON_C  2
#elif defined(ESP32)
#define BUTTON_A 15
#define BUTTON_B 32
#define BUTTON_C 14
#elif defined(ARDUINO_STM32_FEATHER)
#define BUTTON_A PA15
#define BUTTON_B PC7
#define BUTTON_C PC5
#elif defined(TEENSYDUINO)
#define BUTTON_A  4
#define BUTTON_B  3
#define BUTTON_C  8
#elif defined(ARDUINO_FEATHER52832)
#define BUTTON_A 31
#define BUTTON_B 30
#define BUTTON_C 27
#else // 32u4, M0, M4, nrf52840 and 328p
#define BUTTON_A  9
#define BUTTON_B  6
#define BUTTON_C  5
#endif

// Sensor
float ENV_AHT20_TEMP = -1;
float ENV_AHT20_HUM = -1;
float ENV_SGP30_ECO2 = -1;
float ENV_SGP30_TVOC = -1;
float ENV_SGP30_RH2 = -1; // Raw H2
float ENV_SGP30_RETH = -1; // Raw Ethanol
float ENV_CCS811_ECO2 = -1;
float ENV_CCS811_TVOC = -1;

// Statuses
bool STATUS_AHT20 = false;
bool STATUS_SGP30 = false;
bool STATUS_CCS811 = false;
bool STATUS_OLED = false;
bool STATUS_SD = false;

// OLED UI
String NAME = "envlog v0 by Ken S";
String UI_STATUS = "Ready.";
String UI_STATUS_TEMP = "";
int UI_STATUS_TEMP_TIME = 0;

// loop
int loopCounter = 0;

// SD
String buffer;
File dataFile;
String filename = "envlog.csv";
unsigned long lastMillis = 0;
const int chipSelect = 4;

void setup() {
  Serial.begin(9600);
  int c = 0;
  while (!Serial && c < 30) { // turened on at 26; wait for serial for 30 secs
    delay(100);
    Serial.println(c);
    c ++;
  }
  setupOLED();
  setupSD();
  setupSGP30();
  setupAHT20();
  showStatus();
}

void showStatus() {
  String aht20, sgp30, ccs811, oled, sd;
  aht20 = 'x';
  sgp30 = 'x';
  ccs811 = 'x';
  oled = 'x';
  sd = 'x';
  if (STATUS_AHT20) { aht20 = 'v'; };
  if (STATUS_SGP30) { sgp30 = 'v'; };
  if (STATUS_CCS811) { ccs811 = 'v'; };
  if (STATUS_OLED) { oled = 'v'; };
  if (STATUS_SD) { sd = 'v'; };
  displayText("AHT20  " + aht20  + " SGP30  " + sgp30 + "\n" + 
              "CCS811 " + ccs811 + "\n" + 
              "OLED   " + oled   + " SD     " + sd
  );
  delay(1000);
}

void setupSD() {
  // setupSD taken from example code in Arduino IDE, and is in the public domain.
  Serial.println("SD:     Setup");
  Serial.println("SD:     Setup Buffer & LED Pinmode");
  displayText(NAME + "\n\nSD Buffer & Pinmode");
  // reserve 1KiB for String used as a buffer
  buffer.reserve(1024);
  pinMode(LED_BUILTIN, OUTPUT); // SD write indicator
  Serial.println("SD:     Setup SD Card");
  displayText(NAME + "\n\nSD Setup");
  for (int i = 0; i < 100 && !SD.exists(filename); i ++) {
    filename = "envlog" + String(i) + ".csv";
  }
  if (!SD.begin(chipSelect)) { // pin for SD is pin # 4
    Serial.println("SD:     Setup SD Card: Failed/Not Present");
    displayText(NAME + "\n\nSD Setup: Failed");
    delay(1000);
    return;
  }
  Serial.println("SD:     Setup File: Using " + filename);
  displayText(NAME + "\n\nSD Setup file: " + filename);
  Serial.println("SD:     Setup File: Rm if exists " + filename);
  SD.remove(filename);
  dataFile = SD.open(filename, FILE_WRITE);
  Serial.println("SD:     Open File");
  displayText(NAME + "\n\nSD Open File");
  if (!dataFile) {
    Serial.println("SD:     Open File: Error: " + filename);
    displayText(NAME + "\n\nSD Open Error: " + filename);
    delay(2000);
    return;
  }
  Serial.println("SD:     Data: Add Header");
  displayText(NAME + "\n\nSD Add Header");
  dataFile.println("TIME,UI_STATUS,UI_STATUS_TEMP,ENV_AHT20_TEMP,ENV_AHT20_HUM,ENV_SGP30_ECO2,ENV_SGP30_TVOC,ENV_SGP30_RH2,ENV_SGP30_RETH,ENV_CCS811_ECO2,ENV_CCS811_TVOC");
  dataFile.close();
  Serial.println("SD:     OK");
  displayText(NAME + "\n\nSD OK");
  STATUS_SD = true;
}

void setupOLED() {
  Serial.println(NAME);
  // SSD1306_SWITCHCAPVCC = generate display voltage from 3.3V internally
  display.begin(SSD1306_SWITCHCAPVCC, 0x3C); // Address 0x3C for 128x32
  Serial.println("OLED:   Setup");
  display.setTextSize(1);
  display.setTextColor(SSD1306_WHITE);
  displayText(NAME);
  Serial.println("OLED:   Button Pinmodes");
  displayText(NAME + "\n\nOLED Button Pinmodes");
  pinMode(BUTTON_A, INPUT_PULLUP);
  pinMode(BUTTON_B, INPUT_PULLUP);
  pinMode(BUTTON_C, INPUT_PULLUP);
  Serial.println("OLED:   OK");
  displayText(NAME + "\n\nOLED OK");
  STATUS_OLED = true;
}

void setupSGP30() {
  Serial.println("SGP30:  Setup");
  displayText(NAME + "\n\nSGP30");
  if (! sgp.begin()){
    Serial.println("SGP30:  Not Found");
    displayText(NAME + "\n\nSGP30 Not Found");
    delay(1000);
    return;
  }
  Serial.print("SGP30:  Found - #");
  Serial.print(sgp.serialnumber[0], HEX);
  Serial.print(sgp.serialnumber[1], HEX);
  Serial.println(sgp.serialnumber[2], HEX);
  displayText(NAME + "\n\nSGP30 Found");
  // If you have a baseline measurement from before you can assign it to start, to 'self-calibrate'
  sgp.setIAQBaseline(0x8DE3, 0x924A);  // Will vary for each sensor!
  Serial.println("SGP30:  OK");
  displayText(NAME + "\n\nSGP30 OK");
  STATUS_SGP30 = true;
}

void setupAHT20() {
  Serial.println("AHT20:  Setup");
  displayText(NAME + "\n\AHT20");
  if (! aht.begin()){
    Serial.println("AHT20:  Not Found");
    displayText(NAME + "\n\AHT20 Not Found");
    delay(1000);
    return;
  }
  Serial.println("AHT20:  Found:");
  aht_temp = aht.getTemperatureSensor();
  aht_temp->printSensorDetails();

  aht_humidity = aht.getHumiditySensor();
  aht_humidity->printSensorDetails();
  displayText(NAME + "\n\AHT20 Found");
  Serial.println("AHT20:  OK");
  displayText(NAME + "\n\nAHT20 OK");
  STATUS_AHT20 = true;
}

void collectData() {
  Serial.println("SD:     Collect data...");
  unsigned long now = millis();
  if ((now - lastMillis) >= 10) {
    buffer += String(millis()) + "," + UI_STATUS + "," + UI_STATUS_TEMP + "," + 
              String(ENV_AHT20_TEMP) + "," + String(ENV_AHT20_HUM) + "," + 
              String(ENV_SGP30_ECO2) + "," + String(ENV_SGP30_TVOC) + "," + 
              String(ENV_SGP30_RH2) + "," + String(ENV_SGP30_RETH) + "," + 
              String(ENV_CCS811_ECO2) + "," + String(ENV_CCS811_TVOC) + "\n";
    lastMillis = now;
  }
  Serial.println("SD:     Collected data.");
}

void writeNonBlocking() {
  // check if the SD card is available to write data without blocking
  // and if the buffered data is enough for the full chunk size
  dataFile = SD.open(filename, FILE_WRITE);
  unsigned int chunkSize = dataFile.availableForWrite();
  if (chunkSize && buffer.length() >= chunkSize) {
    // write to file and blink LED
    digitalWrite(LED_BUILTIN, HIGH);
    dataFile.write(buffer.c_str(), chunkSize);
    digitalWrite(LED_BUILTIN, LOW);

    // remove written data from buffer
    buffer.remove(0, chunkSize);
  }
  dataFile.close();
}

void displayText(String text) {
  display.clearDisplay();
  display.setCursor(0, 0);
  display.print(text);
  display.display();
}

void displayUI(String text) {
  display.clearDisplay();
  display.setCursor(0, 0);
  display.print(text);
  display.setCursor(0, 24);
  if (UI_STATUS_TEMP_TIME > 0) display.print(UI_STATUS + " " + UI_STATUS_TEMP);
  else display.print(UI_STATUS);
  display.display();
}

String pad(String str, int pad) {
  for(int i = str.length(); i < pad; i++) str += ' ';
  return str;
}

int counter = 0;
void measureSGP30() {
  
  // If you have a temperature / humidity sensor, you can set the absolute humidity to enable the humditiy compensation for the air quality signals
  //float temperature = 22.1; // [°C]
  //float humidity = 45.2; // [%RH]
  //sgp.setHumidity(getAbsoluteHumidity(temperature, humidity));

  if (! sgp.IAQmeasure()) {
    Serial.println("SGP30:  Measure ECO2, TVOC failed");
    ENV_SGP30_TVOC = -1;
    ENV_SGP30_ECO2 = -1;
    return;
  }
  ENV_SGP30_TVOC = sgp.TVOC;
  ENV_SGP30_ECO2 = sgp.eCO2;
  Serial.print("TVOC "); Serial.print(sgp.TVOC); Serial.print(" ppb\t");
  Serial.print("eCO2 "); Serial.print(sgp.eCO2); Serial.println(" ppm");

  if (! sgp.IAQmeasureRaw()) {
    Serial.println("SGP30:  Measure RH2, RETH failed");
    ENV_SGP30_RH2 = -1;
    ENV_SGP30_RETH = -1;
    return;
  }
  ENV_SGP30_RH2 = sgp.rawH2;
  ENV_SGP30_RETH = sgp.rawEthanol;
  Serial.print("RH2  "); Serial.print(sgp.rawH2); Serial.print(" \t");
  Serial.print("RETH "); Serial.print(sgp.rawEthanol); Serial.println("");

  counter++;
  if (counter == 60) {
    counter = 0;

    uint16_t TVOC_base, eCO2_base;
    if (! sgp.getIAQBaseline(&eCO2_base, &TVOC_base)) {
      Serial.println("SGP30:  Failed to get baseline readings");
      return;
    }
    Serial.print("SGP30:  ****Baseline values: eCO2: 0x"); Serial.print(eCO2_base, HEX);
    Serial.print("SGP30:   & TVOC: 0x"); Serial.println(TVOC_base, HEX);
  }
}

void measureAHT20() {
  sensors_event_t humidity;
  sensors_event_t temp;
  aht_humidity->getEvent(&humidity);
  aht_temp->getEvent(&temp);
  ENV_AHT20_TEMP = temp.temperature;
  ENV_AHT20_HUM = humidity.relative_humidity;
  Serial.print("AHT20:  TEMP "); Serial.print(ENV_AHT20_TEMP); Serial.print(" \t");
  Serial.print("AHT20:  HUM  "); Serial.print(ENV_AHT20_HUM); Serial.println("");
  
}

void loop() {
  writeNonBlocking();
  if (loopCounter > 100) {
    loopCounter = 0;
    UI_STATUS_TEMP_TIME = 0;
    // Measure
    measureSGP30();
    measureAHT20();
    collectData();
    String temp = pad(String(ENV_AHT20_TEMP), 5);
    String hum = pad(String(ENV_AHT20_HUM), 5);
    String eco21 = pad(String(int(ENV_SGP30_ECO2)), 5);
    String tvoc1 = pad(String(int(ENV_SGP30_TVOC)), 5);
    String rh21 = pad(String(int(ENV_SGP30_RH2)), 5);
    String reth1 = pad(String(int(ENV_SGP30_RETH)), 5);
    String eco22 = pad(String(int(ENV_CCS811_ECO2)), 5);
    String tvoc2 = pad(String(int(ENV_CCS811_TVOC)), 5);
    String blank = "    ";
    displayUI("eCO2 " + eco21 + " TVOC " + tvoc1 + "\n" + 
              "RH2  " + rh21  + " RETH " + reth1 + "\n" + 
              "TEMP " + temp  + " HUM  " + hum);
  }
  if (!digitalRead(BUTTON_A)) {
    UI_STATUS_TEMP = "Btn A";
    UI_STATUS_TEMP_TIME = 1000;
  }
  if (!digitalRead(BUTTON_B)) {
    UI_STATUS_TEMP = "Btn B";
    UI_STATUS_TEMP_TIME = 1000;
  }
  if (!digitalRead(BUTTON_C)) {
    UI_STATUS_TEMP = "Btn C";
    UI_STATUS_TEMP_TIME = 1000;
  }
  loopCounter ++;
}
